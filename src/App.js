import React, { Component } from "react";
import { connect } from "react-redux";
import { Router, Switch, Route, Link } from "react-router-dom";
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Header from './components/tabs.component';
import StyleBar from './components/navbar.component';
import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardModerator from "./components/board-moderator.component";
import BoardAdmin from "./components/board-admin.component";
import Posts from './components/posts.component';
import CPost from './components/cpost.component';
import sPostsA from './components/sposta.component';
import sComentarioA from './components/comenta.component';
import APosts from './components/apost.component';
import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";
import InformePost from './components/informesa.component';
import { history } from './helpers/history';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  
}));

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showModeratorBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    };

    history.listen((location) => {
      props.dispatch(clearMessage());
    });
  }

  componentDidMount() {
    const user = this.props.user;

    if (user) {
      this.setState({
        currentUser: user,
        showModeratorBoard: user.roles.includes("Moderador"),
        showAdminBoard: user.roles.includes("Administrador"),
      });
    }
  }

  logOut() {
    this.props.dispatch(logout());
  }
  
  render() {
  const { currentUser, showModeratorBoard, showAdminBoard } = this.state;
    return (
      <Router history={history}>
        <div>
          <StyleBar></StyleBar>
        <div className={useStyles.grow}>
          <AppBar style={{ background: '#000000' }} position="static">
            <Toolbar>
              <Icon>warning</Icon>
              <Typography >
                <Link style={{ color: '#FFFFFA' }} to={"/"} className="nav-link">
                  INCIDENTS
                 </Link>
              </Typography>
            <Toolbar>

            </Toolbar> 
            <div class="makeStyles-grow-1"></div>
          <div className={useStyles.grow}>
            {currentUser ? (
            <Toolbar>
                <label>{currentUser.username}</label>
                <button className="nav-link lnbutton" onClick={this.logOut}> Salir </button>
            </Toolbar>) : (
            <Toolbar>
                <Link style={{ color: '#FFFFFA' }} to={"/login"} className="nav-link Button">
                  INGRESAR
                </Link> 
                <Link style={{ color: '#FFFFFF' }} to={"/registro"} className="nav-link">
                  REGISTRATE
                </Link>
            </Toolbar>
                )}
            </div>
            <div className={useStyles.sectionDesktop}>
            </div>

             </Toolbar>
              
            </AppBar>
          {currentUser && (<Header></Header>)}
          </div>
          <div className="container mt-3">
            <Switch>
              <Route exact path={["/", "/inicio"]} component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/registro" component={Register} />
              <Route exact path="/perfil" component={Profile} />
              <Route path="/usuario" component={BoardUser} />
              <Route path="/mod" component={BoardModerator} />
              <Route path="/admin" component={BoardAdmin} />

              <Route path="/post" component={Posts} />
              <Route path="/post_create" component={CPost} />
              <Route path="/post_me_aprov" component={sPostsA} />
              <Route path="/com_me_aut" component={sComentarioA} />
              <Route path="/cuenta" component={BoardModerator} />
              <Route path="/autorizacionpost" component={APosts} />
              <Route path="/informes" component={InformePost} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(App);
