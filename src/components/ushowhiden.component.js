import React, { Component, forwardRef, useRef, useImperativeHandle } from 'react';
import IconButton from '@material-ui/core/IconButton';
import ChatIcon from '@material-ui/icons/Chat';
import Icon from '@material-ui/core/Icon';
import Comentarios from './ucomentarios.component';
import Box from '@material-ui/core/Box';

class Child extends React.Component {
  
     render() {
       return (
           <Comentarios id_post={this.props.id_post2}/>
       );
     }
}

export class ShowHide extends Component {
  
     constructor() {
       super();
       this.state = {
         childVisible: false
       }
     }

     render() { 
       return (
         <div>
           <div onClick={() => this.onClick()}>
                <IconButton aria-label="delete">
                <ChatIcon fontSize="large" />
                </IconButton>
           </div>
           {
             this.state.childVisible
               ? <Child id_post2={this.props.idpost} />
               : null
           }
         </div>
       )
     }

     onClick() {
       this.setState(prevState => ({ childVisible: !prevState.childVisible }));
     }
};

export default ShowHide;