import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import TextArea from 'react-validation/build/textarea';
import CheckButton from "react-validation/build/button";
import { Redirect } from "react-router-dom";
import AuthService from "../services/auth.service";
import Swal from 'sweetalert2';
import PostService from "../services/post.service";

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Este campo es requerido!
      </div>
    );
  }
};

const vlastn = value => {
  if (value.length < 1 || value.length > 120) {
    return (
       <label></label>
    );
  }
};

export default class CPost extends Component
 {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeLastName = this.onChangeLastName.bind(this);
    this.onChangeidusuario = this.onChangeidusuario.bind(this);
    this.onChangeidpost = this.onChangeidpost.bind(this);

    this.state = {
      titulo: "",
      id_usuario: "",
      description: "",
      successful: false,
      message: ""
    };
    
  }

  
  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
    if (!currentUser) this.setState({ redirect: "/inicio" });
    this.setState({ currentUser: currentUser, userReady: true })
  }

  onChangeidpost(e) {
    this.setState({
      titulo: e.target.value
    });
  }
  
  onChangeidusuario(e) {
    this.setState({
      id_usuario: e.target.value
    });
  }

  
  onChangeLastName(e) {
    this.setState({
      description: e.target.value
    });
  }
  

  handleRegister(e) {
    
  e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      PostService.postPostContent(
        this.state.description,
        this.state.titulo,
        this.state.id_usuario
      ).then(
        response => {
          /*this.setState({
            message: ' Post Agregado en espera de Autorizacion.',
            successful: true
          });*/
          Swal.fire({
            title: 'Post Agregado',
            text: "En espera de autorizacion!",
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
          }).then((result) => {
            if (result.isConfirmed) {
               window.location.href = '/post'; 
            }
          })
         
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
    } 
  }

  render() {
    
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    const { currentUser } = this.state;
    return (
        <div class="card-body">
           {!this.state.successful && (
          <Form
            onSubmit={this.handleRegister}
            ref={c => {
              this.form = c;
            }}
          >
           
                 <div className="form-group">
                   <Input
                    type="text"
                    className="form-control"
                    name="titulo"
                    placeholder="Escribe el Titulo"
                     value={this.state.description}
                     onChange={this.onChangeLastName}
                     validations={[required, vlastn]}
                     size="100"
                   />
                </div>

                
                <div className="form-group">
                  <TextArea
                     type="text"
                      className="form-control"
                     name="comentario"
                     placeholder="Escribe la Descripcion..."
                    value={this.state.titulo}
                    onChange={this.onChangeidpost}
                    validations={[required, vlastn]}
                  />
                </div>
              {!this.state.successful && (
              <div>
                        {(this.state.userReady) ?
               <div>
                <div className="form-group">
                  <Input
                    type="hidden"
                    className="form-control"
                    name="id_usuario"
                    placeholder="id_usuario"
                    value={this.state.id_usuario = currentUser.id}
                    onChange={this.onChangeidusuario}
                  />
                </div></div>: null}
                
                
                <div className="form-group">
                   <button className="btn btn-dark  btn-block">Publicar</button>
                </div>
                
              </div>

                
            )}

            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>)}
           {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
      </div>
    );
  }
}