import React, { useRef } from 'react';
import IconButton from '@material-ui/core/IconButton';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import Box from '@material-ui/core/Box';
import PostService from '../services/post.service';
import CancelIcon from '@material-ui/icons/Cancel';

function aceptado (id_post) {
    const autorizado = 1;
    const id_usuario = 2;
    PostService.putPostAuth(
        autorizado,
        id_post,
        id_usuario
      ).then(
        response => {
          console.log(response);
          window.location.href = window.location.pathname + window.location.search + window.location.hash;
         // this.resetComent();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
}

 function denengado(id_post){
    const rechazado = 1;
    const id_usuario = 2;
    PostService.putPostDen(
        rechazado,
        id_post,
        id_usuario
      ).then(
        response => {
            console.log(response);
          window.location.href = window.location.pathname + window.location.search + window.location.hash;
         // this.resetComent();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
  }

export default function FPost({ post }) {
  const ComRef = useRef();

  
  return (
<div className="container mt-4 mx-auto">
    <div className="col-lg-12">
        <div className="card">
        <div className="card-header col-lg-12">
        <Box display="flex" p={1}>
            <Box p={1} width="100%">
            {post.id_usuario['first_name'] } {post.id_usuario['last_name'] }
            </Box>
        </Box>
        </div>
        <div className="card-body">
            <h4 className="card-title"><strong>{post.titulo }</strong></h4>
            <p className="card-text text-justify">{post.description } </p>
        </div>
         <Box display="flex" justifyContent="center" m={1} p={1} >
                      <Box p={1}>
                       <IconButton aria-label="delete" onClick={() => aceptado(post.id)}>
                             <CheckCircleIcon fontSize="large" />
                        </IconButton>
                      </Box>
                      <Box p={1} >
                       <IconButton aria-label="delete" onClick={() => denengado(post.id)}>
                             <CancelIcon fontSize="large" />
                        </IconButton>
                        </Box>
                      </Box>
        </div>
    </div>
</div>
)
}

/*


  aceptado(idc){
      console.log(this.state);
    ComentarioService.putPostComentario(
        idc,
        this.state.autorizado,
        this.state.id_post,
        this.state.id_usuario
      ).then(
        response => {
          this.resetComent();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
  }

  denengado(idc){
        console.log('denegado'+ idc);
        
      console.log(this.state);
    ComentarioService.deletePostComentario(
        idc
      ).then(
        response => {
          this.resetComent();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
  }*/