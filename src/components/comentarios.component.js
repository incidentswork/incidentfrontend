import React, { Component, useRef } from 'react';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroll-component';
import Moment from 'react-moment';
import LoadCircle from './progress.component';
import ComentarioService from "../services/comentario.service";
import Swal from 'sweetalert2';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Box from '@material-ui/core/Box';

import Icon from '@material-ui/core/Icon';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import AuthService from "../services/auth.service";
import authHeader from '../services/auth-header';


const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Este campo es requerido!
      </div>
    );
  }
};

const vlastn = value => {
  if (value.length < 1 || value.length > 120) {
    return (
       <label></label>
    );
  }
};

 
export default class Comentarios extends Component{

  constructor(props) {
    super(props);

    const currentUser = AuthService.getCurrentUser();
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeLastName = this.onChangeLastName.bind(this);
    this.onChangeidusuario = this.onChangeidusuario.bind(this);
    this.onChangeidpost = this.onChangeidpost.bind(this);
    this.state = {
      id_post: this.props.id_post,
      id_usuario: currentUser.id,
      comentario: "",
      comentarios: [],
      start: 1,
      error: 0,
      post: this.props.id_post
    };
  }

  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
    if (!currentUser) this.setState({ redirect: "/inicio" });
    this.setState({ currentUser: currentUser, userReady: true });
    this.fetchComentarios();
  }

  resetComent = () => {
    this.state.comentarios= [];
    this.state.start = 1;
    this.fetchComentarios();
  }

  fetchComentarios = () => {
    const { start } = this.state;
    this.setState({ start: this.state.start + 1 });
    axios
      .get(`http://localhost:8000/api/comentpost/${this.state.post}?page=${start}`,{headers: authHeader()})
      .then(res =>
        
        this.setState({ 
          comentarios: this.state.comentarios.concat(res.data.results) })
      ).catch(err => { 
        this.setState({error: this.state.error + 1})
      });
  };

  
  onChangeidpost(e) {
    this.setState({
      id_post: e.target.value
    });
  }
  
  onChangeidusuario(e) {
    this.setState({
      id_usuario: e.target.value
    });
  }

  onChangeLastName(e) {
    this.setState({
      comentario: e.target.value
    });
  }
  
  handleRegister(e) {
    e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      ComentarioService.postPostComentario(
        this.state.comentario,
        this.state.id_post,
        this.state.id_usuario
      ).then(
        response => {
          //this.resetComent();
          Swal.fire({
            title: 'Exito',
            text: "Comentario en espera de Autorizacion",
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay!'
          }).then((result) => {
            this.resetComent();
          })
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
    }
  }
  

  render() {
    const currentUser = AuthService.getCurrentUser();
    let lab;
    if (this.state.error > 0) {
      lab = 
      <div>
      </div>;
    } else {
      lab = <label><LoadCircle></LoadCircle></label>;    }
    return (
      <div className='container'>

          <InfiniteScroll
          dataLength={this.state.comentarios.length}
          next={this.fetchComentarios}
          hasMore={true}
          loader={<h4>{lab}</h4>}
        >

         {
            this.state.comentarios.map((comentario, i) => {
              
                return (
                    <Card className="mt-2">
                      <CardHeader
                        avatar={
                          <Avatar aria-label="recipe">
                            {comentario.id_usuario['first_name'][0]}
                          </Avatar>
                        }
                        
                      title={<h6>{comentario.id_usuario['first_name']} {comentario.id_usuario['last_name']}</h6>}
                        subheader={<Moment format="ll">{comentario.created_at}</Moment>}
                        />
                      <CardContent>
                        <Typography className="text-justify" color="textSecondary">
                          {comentario.comentario}
                        </Typography>
                      </CardContent>
                    </Card>
                );
            })
         }
          </InfiniteScroll>
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-md-offset-2">
                  <div className="card-body d-flex justify-content-between align-items-center">
                     &nbsp;
                    {this.state.error == 0 && <button className="mt-2 btn btn-dark btn-md" onClick={this.fetchComentarios}>Cargar mas Comentarios</button>}
                    {this.state.error > 0 && <label></label>}
                  </div>
              </div>
            </div>
          </div>
          <div className="card-body">
          <Form
            onSubmit={this.handleRegister}
            ref={c => {
              this.form = c;
            }}
          >
            
            {!this.state.successful && (
              <div>
                <div className="form-group">
                  <Input
                    type="hidden"
                    className="form-control"
                    name="id_post"
                    placeholder="id_post"
                    value={this.state.id_post}
                    onChange={this.onChangeidpost}
                  />
                </div>
                
                <div className="form-group">
                  <Input
                    type="hidden"
                    className="form-control"
                    name="id_usuario"
                    placeholder="id_usuario"
                    value={this.state.id_usuario}
                    onChange={this.onChangeidusuario}
                  />
                </div>
                
                <table>
                  <tr>    
                    <td>
                      <strong>{currentUser.username} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                    </td>
                    <td>
                      <div className="form-group">
                        <Input
                          type="text"
                          className="form-control"
                          name="comentario"
                          placeholder="Escribe un Comentario..."
                          value={this.state.comentario}
                          onChange={this.onChangeLastName}
                          validations={[required, vlastn]}
                          size="100"
                        />
                      </div>
                    </td>
                    <td>
                      <div className="form-group">
                          <button><Icon>send</Icon></button>
                        </div>
                    </td>
                  </tr>
                </table>
                
              </div>
            )}

            {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
      </div>
          
      </div>
    );
  }
}