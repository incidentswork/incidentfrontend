import React, { Component } from 'react';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroll-component';
import FPost from './afpost.component';
import LoadCircle from './progress.component';
import authHeader from '../services/auth-header';

export class APosts extends Component {
  state = {
    post: [],
    start: 1,
    error: 0
  };

  componentDidMount() {
    this.fetchPost();
  }

  fetchPost = () => {
    const { start } = this.state;
    this.setState({ start: this.state.start + 1 });
    axios
      .get(`http://localhost:8000/api/denpag?page=${start}`,{headers: authHeader()})
      .then(res =>
        
        this.setState({ 
          post: this.state.post.concat(res.data.results) })
      ).catch(err => { 
        this.setState({error: this.state.error + 1})
        console.log(this.state.error)
      });
  };


  render() {
    let lab;
    if (this.state.error > 0) {
      lab = 
      <div class="card">
        <div class="card-body">
          <strong><h4>No existen mas datos.</h4></strong>
        </div>
      </div>;
    } else {
      lab = <LoadCircle></LoadCircle>;    }
    return (
      <div className='images'>
          <InfiniteScroll
          dataLength={this.state.post.length}
          next={this.fetchPost}
          hasMore={true}
          loader={<h4>{lab}</h4>}
        >
        
         {this.state.post.map(post => (
            <FPost key={post.id} post={post} />
          ))}
         
          </InfiniteScroll>
          {this.state.err && <button>Fin</button>}
      </div>
    );
  }
}

export default APosts;
