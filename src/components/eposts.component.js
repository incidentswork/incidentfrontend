import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import ChatIcon from '@material-ui/icons/Chat';
import Icon from '@material-ui/core/Icon';
import UPost from './uposta.component';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import Swal from 'sweetalert2';
import PostService from '../services/post.service';

class Child extends React.Component {
  
     render() {
       return (
           <UPost user={this.props.user} titulo={this.props.titulo} descripcion={this.props.descripcion} id_post={this.props.id_post2}/>
       );
     }
}

export class ShowHideP extends Component {
  
     constructor() {
       super();
       this.state = {
         childVisible: false
       }
     }

     
 delete(numberid){
   
   Swal.fire({
      title: 'Borrar?',
      text: "Estas seguro de borrar este Post",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
    PostService.deletePostContent(
     numberid
     ).then(
     response => {

      Swal.fire({
        title: 'Exito',
        text: "Post Borrado",
        icon: 'success',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Okay!',
      }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = window.location.pathname + window.location.search + window.location.hash;
        }
      })
      
    this.setState({
       successful: true
     });
     },
    error => {
     const resMessage =
        (error.response &&
          error.response.detail &&
          error.response.data) ||
          error.message ||
          error.toString();
      this.setState({
                  successful: false,
                  message: resMessage
                });
              }
            );
      }
    })
  }

     render() { 
       return (
         <div>
           
           <Box display="flex" flexDirection="row-reverse" p={1} >
            <Box p={1}>
                <IconButton aria-label="delete" onClick={() => this.delete(this.props.idpost)}>
                    <DeleteIcon fontSize="large" />
                </IconButton>
            </Box> 
            <div onClick={() => this.onClick()}>
            <Box p={1}>
                <IconButton aria-label="delete">
                <BorderColorIcon fontSize="large" />
                </IconButton>
            </Box>
           </div>
        </Box>
           {
             this.state.childVisible
               ? <Child user={this.props.user} titulo={this.props.titulo} descripcion={this.props.descripcion}  id_post2={this.props.idpost} />
               : null
           }
         </div>
       )
     }

     onClick() {
       this.setState(prevState => ({ childVisible: !prevState.childVisible }));
     }
};

export default ShowHideP;