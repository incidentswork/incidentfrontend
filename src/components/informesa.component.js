import React, { Component } from 'react';
import { withStyles, Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import PostService from '../services/post.service';
import AuthService from "../services/auth.service";

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);


export class InformePost extends Component{
state = {
    pauto: 0,
    pden: 0
};

componentDidMount() {
  const currentUser = AuthService.getCurrentUser();
  this.PostAuto(currentUser.id);
  this.PostDeni(currentUser.id);
}

PostAuto = (idu) => {
   PostService.getAuthPost(idu
      ).then(
        response => {
            console.log(response.data.count);
            this.setState({ pauto: response.data.count });
         // this.resetComent();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
  };

PostDeni = (idu) => {
   PostService.getDenied(idu
      ).then(
        response => {
            console.log(response.data.count);
         // this.resetComent();
            this.setState({ pden: response.data.count });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
  };

  render() {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center">Post Autorizados</StyledTableCell>
            <StyledTableCell align="center">Post Rechazados</StyledTableCell>
            <StyledTableCell align="center">Aprobacion</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            <StyledTableCell align="center"><strong><h1>{this.state.pauto}</h1></strong></StyledTableCell>
            <StyledTableCell align="center"><strong><h1>{this.state.pden}</h1></strong></StyledTableCell>
            <StyledTableCell align="center"><strong><h1>{(this.state.pden / this.state.pauto )*100}%</h1></strong></StyledTableCell>
        </TableBody>
      </Table>
    </TableContainer>
  );
}
}


export default InformePost;
