import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  root: {
    colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
  },
}));

export default function LoadCircle() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
            <CircularProgress color="primary" />
    </div>
  );
}