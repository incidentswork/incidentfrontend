import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import TextArea from 'react-validation/build/textarea';
import CheckButton from "react-validation/build/button";
import Swal from 'sweetalert2';

import Icon from '@material-ui/core/Icon';
import PostService from "../services/post.service";

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Este campo es requerido!
      </div>
    );
  }
};

const vlastn = value => {
  if (value.length < 1 || value.length > 120) {
    return (
       <label></label>
    );
  }
};

export default class CPost extends Component
 {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangetitulo = this.onChangetitulo.bind(this);
    this.onChangede = this.onChangede.bind(this);
    this.onChangeidusuario = this.onChangeidusuario.bind(this);

    this.state = {
      titulo: this.props.titulo,
      id_usuario: this.props.user,
      description: this.props.descripcion,
      id_post: this.props.id_post,
      successful: false,
      message: ""
    };
    
  }


  onChangetitulo(e) {
    this.setState({
      titulo: e.target.value
    });
  }
  
  onChangede(e) {
    this.setState({
      description: e.target.value
    });
  }
  
  onChangeidusuario(e) {
    this.setState({
      id_usuario: e.target.value
    });
  }

  
  

  handleRegister(e) {
  e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
        console.log(this.state);
      PostService.putPostContent(
        this.state.titulo,
        this.state.description,
        this.state.id_usuario,
        this.state.id_post
      ).then(
        response => {
          Swal.fire({
              title: 'Exito',
              text: "Actualizacion Correcta!",
              icon: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Okay!'
            }).then((result) => {
              if (result.isConfirmed) {
                window.location.href = window.location.pathname + window.location.search + window.location.hash;
              }
            })
          this.setState({
            successful: true
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );/* */
    }
  }

  render() {
    return (
        <div class="card-body">
          <Form
            onSubmit={this.handleRegister}
            ref={c => {
              this.form = c;
            }}
          >
            
            {!this.state.successful && (
              <div>
                 <div className="form-group">
                   <Input
                    type="text"
                    className="form-control"
                    name="titulo"
                    placeholder="Escribe el Titulo"
                     value={this.state.titulo }
                     onChange={this.onChangetitulo}
                     validations={[required, vlastn]}
                   />
                </div>

                 <div className="form-group">
                   <TextArea
                    type="text"
                    className="form-control"
                    name="description"
                    placeholder="Escribe el description"
                     value={this.state.description }
                     onChange={this.onChangede}
                    validations={[required, vlastn]}
                   />
                </div>

                <div className="form-group">
                  <Input
                    type="hidden"
                    className="form-control"
                    name="id_usuario"
                    placeholder="id_usuario"
                    value={this.state.id_usuario }
                    onChange={this.onChangeidusuario}
                  />
                </div>
                
                
                <div className="form-group">
                   <button className="btn btn-dark  btn-block">Actualizar</button>
                </div>
                
              </div>

                
            )}

            {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
      </div>
    );
  }
}