import React, { useRef } from 'react';

export default function FPostA({ post }) {
  const ComRef = useRef();
  return (
        <div>
            <h4 className="card-title"><strong>{post.titulo }</strong></h4>
            <p className="card-text text-justify">{post.description }</p>
        </div>
        
)
}