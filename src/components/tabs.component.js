import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import React from "react";
import { Tabs } from "@material-ui/core";
import Tab from "@material-ui/core/Tab";
import PersonIcon from '@material-ui/icons/Person';
import PostAddIcon from '@material-ui/icons/PostAdd';
import ListAltIcon from '@material-ui/icons/ListAlt';
import RateReviewIcon from '@material-ui/icons/RateReview';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import DashboardIcon from '@material-ui/icons/Dashboard';
import FolderSharedIcon from '@material-ui/icons/FolderShared';
import { Link } from "react-router-dom";
import AuthService from '../services/auth.service';

const useStyles = makeStyles({
  root: {
    flexGrow: 1
  },
   palette: {
    primary: {
      main: '#f44336',
    },
  },
});

const Header = () => {
  
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const user = AuthService.getCurrentUser();
  return (
    <nav className={classes.root}>
      <AppBar position="static" color="default">
        <Toolbar>
          <Grid justify={"space-between"} container>
            <Grid xs={1} item>
            </Grid>
            <Grid xs={12} item>
              <Grid container justify={"center"}>
                <Tabs
                  onChange={handleChange}
                  value={value}
                  indicatorColor="primary"
                  aria-label="Navigation Tabs"
                >
                    {user&&(<Tab value="/post_create" component={Link} to={"/post_create"} icon={<PostAddIcon />} aria-label="phone"/>)}
                    {user&&(<Tab value="/post_me_aprov" component={Link} to={"/post_me_aprov"} icon={<PlaylistAddCheckIcon />} aria-label="phone"/>)}
                    {user&&(<Tab value="/com_me_aut" component={Link} to={"/com_me_aut"} icon={<RateReviewIcon />} aria-label="phone"/>)}
                    {user&&(<Tab value="/post" component={Link} to={"/post"} icon={<ListAltIcon />} aria-label="favorite" />)}
                    {user&&(<Tab value="/perfil" component={Link} to={"/perfil"} icon={<PersonIcon />} aria-label="person" />)}
                    {user.roles.includes("Administrador")&&(<Tab value="/autorizacionpost" component={Link} to={"/autorizacionpost"} icon={<DashboardIcon />} aria-label="phone"/>)}
                    {user.roles.includes("Administrador")&&(<Tab value="/informes" component={Link} to={"/informes"} icon={<FolderSharedIcon />} aria-label="phone"/>)}
                </Tabs>
              </Grid>
            </Grid>
            <Grid item xs={1} />
          </Grid>
        </Toolbar>
      </AppBar>
    </nav>
  );
};

export default Header;

