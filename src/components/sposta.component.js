import React, { Component, useRef } from 'react';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroll-component';
import FPostA from './fposta.component';
import LoadCircle from './progress.component';
import ShowHideP from './eposts.component';
import DeleteIcon from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import PostService from '../services/post.service';
import AuthService from "../services/auth.service";
import authHeader from '../services/auth-header';

export class sPostsA extends Component {
  state = {
    post: [],
    start: 1,
    error: 0,
    reload: false
  };

  
  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();
    if (!currentUser) this.setState({ redirect: "/inicio" });
    this.setState({ currentUser: currentUser, userReady: true });
    this.fetchPost();
  }

  fetchPost = () => {
    const currentUser = AuthService.getCurrentUser();
    const { start } = this.state;
    this.setState({ start: this.state.start + 1 });
    axios
      .get(`http://localhost:8000/api/me_post/${currentUser.id}?page=${start}`,{headers: authHeader()})
      .then(res =>
        
        this.setState({ 
          post: this.state.post.concat(res.data.results) })
      ).catch(err => { 
        this.setState({error: this.state.error + 1})
        console.log(this.state.error)
      });
  };


  reset(){
    window.location.href = window.location.pathname + window.location.search + window.location.hash;
  }
  

  render() {
    const { currentUser } = this.state;
    let lab;
    if (this.state.error > 0) {
      lab = 
      <div>
        {/*<div class="card-body">
          <strong><h4>No existen mas datos.</h4></strong>
    </div>*/}
      </div>;
    } else {
      lab = <Box display="flex" justifyContent="center" m={1} p={1} bgcolor="background.paper"><LoadCircle></LoadCircle></Box>;    }
    return (
      <div className='images'>
          <InfiniteScroll
          dataLength={this.state.post.length}
          next={this.fetchPost}
          hasMore={true}
          loader={<h4>{lab}</h4>}
        >
        
         {this.state.post.map(post => (
           
      <div className="container mt-4 mx-auto">
          <div className="col-lg-12">
              <div className="card">  
              <div className="card-body">
            <FPostA key={post.id} post={post} />
            <Box display="flex" flexDirection="row-reverse" p={1} >
            <div onClick={() => this.onClick()}>

            </div>
            </Box>
            <ShowHideP user={currentUser.id} titulo={post.titulo} descripcion={post.description} idpost={post.id}></ShowHideP>
            </div>
          </div>
        </div>
    </div>
          ))}

          
         
          </InfiniteScroll>
          {this.state.err && <button>Fin</button>}
      </div>
    );
  }
}

export default sPostsA;
