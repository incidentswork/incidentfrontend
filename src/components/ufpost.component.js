import React, { useRef } from 'react';
import Moment from 'react-moment';
import ShowHide from './ushowhiden.component';
import Box from '@material-ui/core/Box';

export default function FPost({ post }) {
  const ComRef = useRef();
  return (
<div className="container mt-4 mx-auto">
    <div className="col-lg-12">
        <div className="card">
        <div className="card-header col-lg-12">
        <Box display="flex" p={1}>
            <Box p={1} width="100%">
            <h4 className="card-title"><strong>{post.titulo }</strong></h4>         
            </Box>
            <Box p={1} flexShrink={0}>
            <Moment format="DD/MM/YYYY">{post.created_at}</Moment>
            </Box>
        </Box>
        </div>
        <div className="card-body"> 
        <div className="text-left">
        <ShowHide ref={ComRef} idpost={post.id}></ShowHide>
        </div>
        </div>
        </div>
    </div>
</div>
)
}