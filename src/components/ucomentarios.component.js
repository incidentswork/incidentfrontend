import React, { Component, useRef } from 'react';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroll-component';
import Moment from 'react-moment';
import LoadCircle from './progress.component';
import IconButton from '@material-ui/core/IconButton';
import ComentarioService from "../services/comentario.service";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import SpeakerNotesOffIcon from '@material-ui/icons/SpeakerNotesOff';
import authHeader from '../services/auth-header';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Box from '@material-ui/core/Box';

import Icon from '@material-ui/core/Icon';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

 
export default class Comentarios extends Component{

  constructor(props) {
    super(props);

    this.state = {
      autorizado: 1,
      id_post: this.props.id_post,
      id_usuario: 1,
      comentarios: [],
      start: 1,
      error: 0,
      post: this.props.id_post
    };
  }

  componentDidMount() {
    this.fetchComentarios();
  }

  resetComent = () => {
    this.state.comentarios= [];
    this.state.start = 1;
    this.fetchComentarios();
  }

  fetchComentarios = () => {
    const { start } = this.state;
    this.setState({ start: this.state.start + 1 });
    axios//http://localhost:8000/api/dcomentpost/123?page=1  http://localhost:8000/api/comentpost/${this.state.post}?page=${start}
      .get(`http://localhost:8000/api/dcomentpost/${this.state.post}?page=${start}`,{headers: authHeader()})
      .then(res =>
        
        this.setState({ 
          comentarios: this.state.comentarios.concat(res.data.results) })
      ).catch(err => { 
        this.setState({error: this.state.error + 1})
      });
  };


  aceptado(idc){
      console.log(this.state);
    ComentarioService.putPostComentario(
        idc,
        this.state.autorizado,
        this.state.id_post,
        this.state.id_usuario
      ).then(
        response => {
          this.resetComent();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
  }

  denengado(idc){
        console.log('denegado'+ idc);
        
      console.log(this.state);
    ComentarioService.deletePostComentario(
        idc
      ).then(
        response => {
          //this.resetComent();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.detail &&
              error.response.data) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
  }
  

  render() {
    let lab;
    if (this.state.error > 0) {
      lab = 
      <div>
      </div>;
    } else {
      lab = <label><LoadCircle></LoadCircle></label>;    }
    return (
      <div className='container'>
        {/*<Box display="flex" flexDirection="row-reverse" p={1} m={1} >
          <Box p={1} >
            <button className="mt-2 btn btn-primary btn-md" onClick={this.resetComent}>Reset</button>
          </Box>
    </Box>*/}

          <InfiniteScroll
          dataLength={this.state.comentarios.length}
          next={this.fetchComentarios}
          hasMore={true}
          loader={<h4>{lab}</h4>}
        >

         {
            this.state.comentarios.map((comentario, i) => {
              
                return (
                    <Card className="mt-2">
                      <CardHeader
                        avatar={
                          <Avatar aria-label="recipe">
                            {comentario.id_usuario['first_name'][0]}
                          </Avatar>
                        }
                      title={<h6>{comentario.id_usuario['first_name']} {comentario.id_usuario['last_name']}</h6>}
                        subheader={<Moment format="ll">{comentario.created_at}</Moment>}
                        />
                      <CardContent>
                        <Typography className="text-justify" color="textSecondary">
                            {comentario.comentario}
                        </Typography>
                      </CardContent>
                      <Box display="flex" justifyContent="center" m={1} p={1} bgcolor="background.paper">
                      <Box p={1}>
                       <IconButton aria-label="delete" onClick={() => this.aceptado(comentario.id)}>
                             <CheckCircleIcon fontSize="large" />
                        </IconButton>
                      </Box>
                      <Box p={1} >
                       <IconButton aria-label="delete" onClick={() => this.denengado(comentario.id)}>
                             <SpeakerNotesOffIcon fontSize="large" />
                        </IconButton>
                        </Box>
                      </Box>
                    </Card>
                );
            })
         }
          </InfiniteScroll>
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-md-offset-2">
                  <div className="card-body d-flex justify-content-between align-items-center">
                     &nbsp;
                    {this.state.error == 0 && <button className="mt-2 btn btn-dark btn-sm" onClick={this.fetchComentarios}>Cargar mas Comentarios</button>}
                    {this.state.error > 0 && <div></div>}
                    {/*/*<button className="mt-2 btn btn-dark btn-sm">Fin</button>*/}
                  </div>
              </div>
            </div>
          </div>
          
      </div>
    );
  }
}