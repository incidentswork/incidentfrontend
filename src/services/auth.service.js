import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:8000/api/";

class AuthService {
    login(username, password) {
        return axios
            .post(API_URL + "login/", {
                username,
                password
            })
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }

                return response.data;
                //console.log(response);
            });
    }

    logout() {
        return axios.get(API_URL + 'logout/', { headers: authHeader() })
        .then(response => {
            window.location.href ="/login";
            localStorage.removeItem("user");
        }); 
    }

    register(username, first_name, last_name, email, password) {
        return axios.post(API_URL + "registrar/", {
            username,
            first_name,
            last_name,
            email,
            password
        });
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem('user'));;
    }
}

export default new AuthService();