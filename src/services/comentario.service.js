import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8000/api/';

class ComentarioService {
  getPostComentario() {
    return axios.get(API_URL + 'public', {headers: authHeader()});
  }
  
  postPostComentario(comentario, id_post, id_usuario) {
      return axios.post(API_URL + "comentario", {
          comentario,
          id_post,
          id_usuario
      }, {headers: authHeader()});
  }

  putPostComentario(id_comentario,autorizado, id_post, id_usuario) {
      return axios.put(API_URL + "comentario/" + id_comentario, {
          autorizado,
          id_post,
          id_usuario
      },{headers: authHeader()});
  }

  deletePostComentario(id_comentario) {
    return axios.delete(API_URL + 'comentario/'+id_comentario, {headers: authHeader()});
  }
}

export default new ComentarioService();
