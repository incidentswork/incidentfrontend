import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8000/api/';

class PostService {

    getAuthPost(id_usuario){
        return axios.get(API_URL + "countpost/"+ id_usuario +"?pag=1", {headers: authHeader()});
    }

    getDenied(id_usuario){
        return axios.get(API_URL + "dcountpost/"+ id_usuario +"?pag=1", {headers: authHeader()});
    }


  postPostContent(titulo, description, id_usuario) {
      return axios.post(API_URL + "post"
          , {
          titulo,
          description,
          id_usuario
      },{headers: authHeader()});
  }

  putPostContent(titulo, description, id_usuario, id_post) {
      return axios.put(API_URL + "post/"+ id_post, {
          titulo,
          description,
          id_usuario
      },{headers: authHeader()});
  }
  
  
  putPostAuth(autorizado, id_post, id_usuario) {
      return axios.put(API_URL + "post/"+ id_post, {
          autorizado,
          id_post,
          id_usuario
      },{headers: authHeader()});
  }

  
  putPostDen(rechazado, id_post, id_usuario) {
      return axios.put(API_URL + "post/"+ id_post, {
          rechazado,
          id_post,
          id_usuario
      },{headers: authHeader()});
  }

  deletePostContent(id_post) {
      return axios.delete(API_URL + "post/"+ id_post ,{headers: authHeader()});
  }

}

export default new PostService();